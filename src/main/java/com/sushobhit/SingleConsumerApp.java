package com.sushobhit;

import com.sushobhit.common.KafkaProperties;
import com.sushobhit.consumer.Consumer;

public class SingleConsumerApp {
	public static void main(String[] args) {
		Consumer consumerThread = new Consumer(KafkaProperties.TOPIC1,"Consumer-1");
		consumerThread.start();
	}

}
