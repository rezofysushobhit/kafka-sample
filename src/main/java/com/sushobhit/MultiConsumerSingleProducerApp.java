package com.sushobhit;

import com.sushobhit.common.KafkaProperties;
import com.sushobhit.consumer.Consumer;
import com.sushobhit.producer.Producer;

public class MultiConsumerSingleProducerApp {

	public static void main(String[] args) throws Exception {

		//true
		boolean isAsync = false;
		Producer producerThread = new Producer(KafkaProperties.TOPIC1, isAsync);
		producerThread.start();

		Consumer consumerThread1 = new Consumer(KafkaProperties.TOPIC1,"Consumer-1");
		consumerThread1.start();
		
		Consumer consumerThread2 = new Consumer(KafkaProperties.TOPIC1,"Consumer-2");
		consumerThread2.start();
		
	}
}
